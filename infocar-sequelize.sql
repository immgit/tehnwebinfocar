CREATE DATABASE  IF NOT EXISTS `infocarv2-sequelize` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `infocarv2-sequelize`;
-- MySQL dump 10.13  Distrib 5.6.24, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: infocarv2-sequelize
-- ------------------------------------------------------
-- Server version	5.6.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cars`
--

DROP TABLE IF EXISTS `cars`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cars` (
  `id` char(36) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL DEFAULT '',
  `nr_inmatriculare` varchar(255) DEFAULT NULL,
  `tip_auto` varchar(255) DEFAULT NULL,
  `marca` varchar(255) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `data_fabricatie` datetime DEFAULT NULL,
  `alte_info` text,
  `km` int(11) DEFAULT NULL,
  `iesire_garantie` datetime DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `userId` char(36) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nr_inmatriculare` (`nr_inmatriculare`),
  KEY `userId` (`userId`),
  CONSTRAINT `cars_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cars`
--

LOCK TABLES `cars` WRITE;
/*!40000 ALTER TABLE `cars` DISABLE KEYS */;
INSERT INTO `cars` VALUES ('5dd3f3d9-3747-48d7-b34f-baf741f0f224','IF-22-MGX','Cabrio','Audi','A6','2015-02-17 22:00:00','Stare perfecta',240000,'2017-01-12 22:00:00','2016-01-15 18:56:18','2016-01-15 18:56:18','b98f5d2f-533e-4ce7-97e3-bbac9e4f7100'),('b755286b-60d2-4001-8609-8ae07101309a','IF-22-MGI','Berlina','Audi','A4','2015-06-22 21:00:00','Stare perfecta',24000,'2016-08-18 21:00:00','2016-01-15 18:55:57','2016-01-15 18:55:57','b98f5d2f-533e-4ce7-97e3-bbac9e4f7100'),('cd05419c-0aec-471c-bc17-9192103a2508','B-09-TEH','SUV','Audi','Q7','2014-08-19 21:00:00','Stare perfecta',240567,'2017-06-29 21:00:00','2016-01-15 18:56:46','2016-01-15 18:56:46','b98f5d2f-533e-4ce7-97e3-bbac9e4f7100');
/*!40000 ALTER TABLE `cars` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_car`
--

DROP TABLE IF EXISTS `service_car`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_car` (
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `carId` char(36) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL DEFAULT '',
  `serviceId` char(36) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`carId`,`serviceId`),
  KEY `serviceId` (`serviceId`),
  CONSTRAINT `service_car_ibfk_1` FOREIGN KEY (`carId`) REFERENCES `cars` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `service_car_ibfk_2` FOREIGN KEY (`serviceId`) REFERENCES `services` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_car`
--

LOCK TABLES `service_car` WRITE;
/*!40000 ALTER TABLE `service_car` DISABLE KEYS */;
/*!40000 ALTER TABLE `service_car` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `services`
--

DROP TABLE IF EXISTS `services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `services` (
  `id` char(36) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL DEFAULT '',
  `nume` varchar(255) DEFAULT NULL,
  `adresa` varchar(255) DEFAULT NULL,
  `telefon` varchar(255) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `services`
--

LOCK TABLES `services` WRITE;
/*!40000 ALTER TABLE `services` DISABLE KEYS */;
INSERT INTO `services` VALUES ('3f586ade-851c-4b31-bec6-427e56f75c20','Reparam Orice','Baneasa nr 23','0212668456','2016-01-06 16:48:41','2016-01-06 16:48:41'),('445f62ca-8018-4f76-a626-e80fba1ce92b','AMI AUTO','Baneasa nr 23','0212668456','2016-01-06 16:47:26','2016-01-06 16:47:26'),('471469b5-9021-40a0-84ea-3a2386be5856','AMI AUTO','Baneasa nr 23','0212668456','2016-01-06 16:48:17','2016-01-06 16:48:17');
/*!40000 ALTER TABLE `services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` char(36) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL DEFAULT '',
  `username` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` text,
  `ip` varchar(255) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES ('b98f5d2f-533e-4ce7-97e3-bbac9e4f7100','immg','immg@immg.ro','2b7ff2a96550ea542f576a1ed8a1ca5fd315647cc0c5ad5008afd44db156d66f045aeed4edbf59f7edfbc30afb2037e780c00764fcbb0f8d6e83f02714ad9fbe','10.10.10.10','2016-01-15 18:54:43','2016-01-15 18:54:43');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-01-15 21:33:54
