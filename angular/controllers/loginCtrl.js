angular.module('infoCarV2')
    .controller('loginCtrl',function($scope,$http,$location,userBackEnd){
        $scope.login = function(){
            if($scope.loginForm.$valid)
            {
                var user = $scope.user;

                userBackEnd.loginUser(user);

            }
        }
    });
