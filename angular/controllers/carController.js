

angular.module('infoCarV2')
    .controller('carController',function($scope,carsService){
        $scope.mesaj = "Adauga masina";
        var CARS;
        var idUser = "";
        refresh();
        function refresh() {
            carsService.getSession().then(function (response) {
                idUser = response.data.id;
                carsService.getCarsPerUser(idUser).then(function (response) {
                    $scope.cars = response.data.cars;
                    CARS = response.data.cars;
                });
            });
        }

        $scope.refresh = function () {
            refresh();
        };

        $scope.addCar = function(){
            var newCar = $scope.newCar;
            newCar.userId = idUser;

            carsService.addCar(newCar).then(function(response){
                $scope.mesaj = response.data.mesaj;
                response.data.ok ? $scope.style = {color: "green"} : $scope.style = {color: "red"};

            });

            refresh();

        };

        $scope.deleteCar = function(id)
        {
            carsService.deleteCar(id).then(function(response){
                if(response.data.ok)
                {
                    $scope.stergereMesaj = "Masina stearsa cu succes!"; //nu afisez inca pe pagina
                    refresh();
                }
            });
        };

        $scope.toggle = function() {
            $scope.show = !$scope.show;
        };

    });

