angular.module('infoCarV2')
    .controller('signupCtrl', function($scope,userBackEnd){

        $scope.validation = {
            word: /^\s*\w*\s*$/
        };
        $scope.signup = function(){
            $scope.mesaj = "";
            var user = {
                username: $scope.user.username,
                email: $scope.user.email,
                password: $scope.user.password,
                ip : "10.10.10.10"
            };

                if($scope.signupForm.$valid && ($scope.user.password === $scope.user.passwordAgain)) {
                    userBackEnd.postUser(user).then(function (response) {
                        //console.log(response);
                       $scope.mesaj = response.data.mesaj;
                    });
                }
            else
                $scope.mesaj = "Parolele nu se potrivesc";

            }

    });