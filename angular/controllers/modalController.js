angular.module('infoCarV2').controller('editCarModal', function ($scope, $uibModal, $log, carsService) {

    $scope.getC = function(car){
        var data_fabricatie = new Date(car.data_fabricatie);
        var iesire_garantie = new Date(car.iesire_garantie);
        var modalCar = car;
        modalCar.data_fabricatie = data_fabricatie;
        modalCar.iesire_garantie = iesire_garantie;
        $scope.car = modalCar;
    };

    $scope.open = function (size) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'myModalContent.html',
            controller: 'ModalInstanceCtrl',
            size: size,
            resolve: {
                carM: function () {
                    return $scope.car;
                }
            }
        });

        modalInstance.result.then(function (carM) {
            carsService.deleteCar($scope.car.id).then(function(response){
                if(response.data.ok)
                {
                    //$scope.stergereMesaj = "Masina stearsa cu succes!"; //nu afisez inca pe pagina

                    carsService.getSession().then(function(response){
                        var id = response.data.id;
                        var updatedCar = carM;
                        updatedCar.userId = id;
                        carsService.addCar(updatedCar).then(function(response){

                            if(response.data.ok)
                            {
                                carsService.getCarsPerUser(id).then(function (response) {
                                    $scope.cars = response.data.cars;
                                });
                            }
                        });

                    });

                }
            });



        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };


});

angular.module('infoCarV2').controller('ModalInstanceCtrl', function ($scope, $uibModalInstance, carM) {

    $scope.carM = carM;

    $scope.ok = function () {
        $uibModalInstance.close($scope.carM);
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});