angular.module('infoCarV2')
    .service('userBackEnd',function($http,$window){
        var link = "http://home.immmgit.ro/";
        var linkLocal = "http://localhost:3000/";
        this.postUser = function(user){
            var promise = $http.post(linkLocal +  'api/user',user);
             return promise;
        };


        this.loginUser = function(user){
            $http.post(linkLocal +  'api/user/login', user).then(function(response){
                if(response.data.ok)
                {
                    $http.post(linkLocal +  'login',response.data).then(function(response){
                        $window.location.href = '/';
                    });
                }
                return false;
            });
        };



    });