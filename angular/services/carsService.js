angular.module('infoCarV2')
    .service('carsService',function($http,$cookies){

        var link = "http://localhost:3000/";

        this.getSession = function(){
            var promise = $http.get(link + 'api/session/user');
            return promise;
        };

        this.getCarsPerUser = function(id){
          var promise = $http.get(link + 'api/cars/user/' + id);
            return promise;
        };

        this.addCar = function(newCar){
            var promise = $http.post(link + 'api/cars', newCar);
            return promise;
        };

        this.deleteCar = function(id){
            var promise = $http.delete(link + 'api/cars/' + id);
            return promise;
        };

    });