var crypto = require('crypto');
var algorithm = 'aes-256-ctr';

module.exports = {
    encrypt: function (text) {
        var cipher = crypto.createCipher(algorithm, text);
        var crypted = cipher.update(text, 'utf8', 'hex');
        crypted += cipher.final('hex');
        return crypted;
    },

    decrypt: function (text) {
        var decipher = crypto.createDecipher(algorithm, text);
        var dec = decipher.update(text, 'hex', 'utf8');
        dec += decipher.final('utf8');
        return dec;
    },

    toSHA512: function(text){
        var textSHA512 = crypto.createHash("sha512").update(text).digest("hex");
        return textSHA512;
    }
};