
Sequelize = require('sequelize');

sequelize =  new Sequelize('infocarv2-sequelize','root','',{
    host: 'localhost',
    dialect: 'mysql',
    pool: {
        max: 5,
        min: 0,
        idle: 10000
    }
});
module.exports = {
    User : sequelize.define('user', {
        id: {type: Sequelize.UUID, defaultValue: Sequelize.UUIDV4, primaryKey: true},
        username: {type: Sequelize.STRING, unique: true},
        email: {type: Sequelize.STRING, unique: true, validate: {isEmail: true}},
        password: Sequelize.TEXT,
        ip: {type:Sequelize.STRING, validate: {isIP: true}}

    }),

    Car : sequelize.define('car',{
        id: {type: Sequelize.UUID, defaultValue: Sequelize.UUIDV4, primaryKey: true},
        nr_inmatriculare : {type: Sequelize.STRING, unique: true},
        tip_auto : {type: Sequelize.STRING},
        marca: {type: Sequelize.STRING},
        model : {type: Sequelize.STRING},
        data_fabricatie : {type: Sequelize.DATE},
        alte_info : {type: Sequelize.TEXT},
        km : {type: Sequelize.INTEGER},
        iesire_garantie: {type: Sequelize.DATE}
    }),

    Service : sequelize.define('service',{
        id : {type: Sequelize.UUID, defaultValue : Sequelize.UUIDV4, primaryKey: true},
        nume : {type: Sequelize.STRING},
        adresa : {type : Sequelize.STRING},
        telefon : {type : Sequelize.STRING}
    }),

    sync: function(){

        this.User.hasMany(this.Car);
        this.Car.belongsTo(this.User);

        this.Service.belongsToMany(this.Car, {foreignKey: "serviceId", through: "service_car"});
        this.Car.belongsToMany(this.Service, {foreignKey: "carId", through: "service_car"});




        sequelize.sync({});

    },

};



