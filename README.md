# README #

Aplicatie Node.JS, AngularJS care doreste sa stocheze intr-o baza de date MySQL date despre useri si masini.

# Parte de back-end #

* Partea de server este facuta cu Node.JS si are in spate un API pe baza RESTful cu protocol HTTP de comunicare (GET, POST, PUT si DELETE).
* In acelasi timp, peste Node.JS este framework-ul Express care usureaza lucrul si permite folosirea de rute si adaugare de noi module.
* Baza de date este una MySQL cu ORM Sequelize.

# Parte de front-end #

* Folosesc framework-ul AngularJS.
* Cu Angular interactionez cu API-ul si fac diferite actiuni pe baza de date.

# Ce isi propune proiectul #

Un utilizator al aplicatiei isi poate crea un cont, iar dupa ce se autentifica poate adauga masini in contul sau, le poate edita sau sterge.
Ideea de baza este ca un client isi poate stoca datele masinii, astfel incat ele vor fi la loc sigur.

----------------------------------------

Baza de date se va genera automat in cazul in care nu exista sau se va sincroniza daca exista. Totusi, in repo exista un fisier .sql cu baza de date exportat. Partea de car service NU este implementata.

----------------------------------------

# Limbaje, Tehnologii si Framework-uri folosite #

Aplicatia are la baza limbajul JavaScript rulat pe parte de server cu ajutorul Node.js. Peste Node.js exista framework-ul Express care simplifica scrierea aplicatiei web.
Motorul de randare a template-urilor este EJS.
Pe parte de front-end exista framework-ul AngularJS, cu ajutorul caruia se comunica cu API-ul prin mesaje de tip RESTful prin protocolul HTTP. Toate comunicarile cu server-ul se fac asincron, cu ajutorul AJAX (Asynchronous JavaScript and XML).
Baza de date este una MySQL cu ORM Sequelize, se aplica paradigma "code-first".