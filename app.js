var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var Sequelize = require('sequelize');
var mysql = require('mysql');
var engine = require('express-ejs-layouts');

var routes = require('./routes/index');
var masini = require('./routes/masini');
var users = require('./routes/users');
var api = require('./routes/api');
var car = require('./routes/car');
var login = require('./routes/login');
var signup = require('./routes/signup');
var logout = require('./routes/logout');
var mycars = require('./routes/mycars');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.set('layout','layout');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(session({
      secret: '1234567890!@#$%^&*()',
      resave: false,
      saveUninitialized: true,
}));
app.use(express.static(path.join(__dirname, 'angular')));
app.use(engine);

app.use(function(req,res,next){
  res.locals.session = req.session;
  next();
});


app.use('/', routes);
app.use('/masini',masini);
app.use('/users',users);
app.use('/api',api);
app.use('/car',car);
app.use('/login',login);
app.use('/signup',signup);
app.use('/logout',logout);
app.use('/mycars',mycars);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});




module.exports = app;
