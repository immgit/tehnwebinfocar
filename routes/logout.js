var express = require('express');
var router = express.Router();

router.get('/',function(req,res,next){
    if(req.session.user)
    {
        req.session.destroy();
        res.clearCookie("user");
        res.redirect('/');
    }
    else {
        res.redirect('/');
    }
});


module.exports = router;