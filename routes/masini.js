var express = require('express');
var router = express.Router();


router.get('/', function(req, res, next) {
    //res.render('index', { title: 'Express' });
    var d = new Date();
    var masina1 = {
            numar_inmatriculare: 'IF01MGI',
            tip_auto: 'Autoturism',
            marca: 'Ford',
            model: 'Fiesta',
            an_fabricatie: '2011',
            alte_info: 'Masina se afla intr-o stare foarte buna',
            km: '240000',
            iesire_garantie: d.toDateString(),
            service:[
                {
                    data: d.toDateString(),
                    tip_service: 'ITP',
                    km_la_bord: '120567',
                    locatia_service: 'Bucuresti',
                    mecanic: 'Ionel',
                    satisfactie: 'multumit',
                    cost: '120'
                },
                {
                    data: d.toDateString(),
                    tip_service: 'ITP',
                    km_la_bord: '156789',
                    locatia_service: 'Arad',
                    mecanic: 'Marian',
                    satisfactie: 'destul de bine',
                    cost: '540'
                }
                ],
            rca:{
                data_expirare: d.toDateString(),
                cost: '240'
            },
            itp: {
                data_expirare: d.toDateString(),
                cost: '160'
            }


        };


    var masina2 = JSON.parse(JSON.stringify(masina1));
    masina2.model = 'A6';
    masina2.marca = 'Audi';
    masina2.km = '150678';
    masina2.numar_inmatriculare = 'B08DGO';
    var masina3 = JSON.parse(JSON.stringify(masina1));
    var masina4 = JSON.parse(JSON.stringify(masina1));
    var masina5 = JSON.parse(JSON.stringify(masina1));
    var masina6 = JSON.parse(JSON.stringify(masina1));
    var masina7 = JSON.parse(JSON.stringify(masina1));
    var masina8 = JSON.parse(JSON.stringify(masina1));
    var masina9 = JSON.parse(JSON.stringify(masina1));
    var masina10 = JSON.parse(JSON.stringify(masina1));
    var masina11= JSON.parse(JSON.stringify(masina1));

    var masini = [{
        response: [
            masina1,
            masina2,
            masina3,
            masina4,
            masina5,
            masina6,
            masina7,
            masina8,
            masina9,
            masina10,
            masina11
        ]
    }];
    res.json(masini);
});

module.exports = router;