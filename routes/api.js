var express = require('express');
var routes = express.Router();
var db_seq = require('../models/sequelize-infocarv2');
var cryp = require('../models/cryp');
db_seq.sync();

routes.post('/user/login',function(req,res,next){
    var email = req.body.email;
    var password = req.body.password;
    var passwordSHA = cryp.toSHA512(password);
    db_seq.User.find({where: {email:email}}).then(function(response){
        if(response == null){
            res.json({ok:false});

        }else {
            if (response.password === passwordSHA) {
                res.json({ok:true,id:response.id,email:response.email});
            }
            else
                res.json({ok:false});
        }
    });
});

routes.get('/cars/:id', function (req, res, next) {
    var id = req.params.id;

    db_seq.Car.findByPrimary(id).then(function(car){
        if(car==null)
            res.json({ok:false});
        else
            res.json(car);
    });
});


routes.post('/cars', function (req, res, next) {
    var object = {
        nr_inmatriculare: req.body.nr_inmatriculare,
        tip_auto: req.body.tip_auto,
        marca: req.body.marca,
        model: req.body.model,
        data_fabricatie: req.body.data_fabricatie,
        alte_info: req.body.alte_info,
        km: req.body.km,
        iesire_garantie: req.body.iesire_garantie,
        userId: req.body.userId
    };

    db_seq.Car.findOrCreate({where: {nr_inmatriculare:object.nr_inmatriculare}, defaults:object})
        .spread(function(car,created){
            var status = {};
            if(!created)
                status = {ok:created,mesaj: "O masina cu acelasi numar de inamtriculare exista deja!"};
            else
                status = {ok:created,mesaj: "Masina adaugata cu succes!"};
            res.send(status);})
        .catch(function(err){res.send(err)});
});


routes.post('/user', function (req, res, next) {
    var password = req.body.password;
    var passwordSHA512 = cryp.toSHA512(password);
    var object = {
        username: req.body.username,
        email: req.body.email,
        password: passwordSHA512,
        ip: req.body.ip
    };


    db_seq.User.findOrCreate({where: {$or: [{username: req.body.username}, {email: req.body.email}]}, defaults: object})
        .spread(function(user,created){
            var status = {};
            if(!created)
            status = {ok:created,mesaj: "Username sau email exista deja!"};
            else
            status = {ok:created,mesaj: "Cont creat cu succes!"};
            res.json(status);})
        .catch(function(err){
            res.send(err);
        });

});

routes.get('/user/:id',function(req,res,next){
    var id = req.params.id;

    db_seq.User.findByPrimary(id).then(function(user){
        var response = {username: user.username, email: user.email};
        res.json(response);
    });

});

routes.get('/cars/user/:id',function(req,res,next){
    var id = req.params.id;
    var query = {
        where: {id:id},
        include: [db_seq.Car]
    };
    db_seq.User.find(query).then(function(resp){res.json(resp)});
});

routes.delete('/cars/:id',function(req,res,next){
    var id = req.params.id;
    db_seq.Car.findByPrimary(id).then(function(car){
        return car.destroy();
    }).then(function(){res.json({ok : true})});
});

routes.post('/service',function(req,res,next){
    var object = {
        nume : req.body.nume,
        adresa : req.body.adresa,
        telefon : req.body.telefon
    };
    db_seq.Service.create(object)
        .then(function(service){service.save();res.send(service)})
        .catch(function(err){res.send(err)});
});

routes.get('/session/user',function(req,res,next){
    res.json(req.session.user);
});





module.exports = routes;