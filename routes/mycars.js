var express = require('express');
var router = express.Router();

router.get('/',function(req,res,next){
   if(typeof req.session.user == 'undefined')
   {
       res.redirect('/');


   }
    else {
       res.render('mycars',{user: req.session.user});

   }
});

module.exports = router;