var express = require('express');
var router = express.Router();

router.get('/',function(req,res,next){
    if(req.session.email)
    {
        res.redirect('/');

    }
    else {
        res.render('login');
    }
});

router.post('/',function(req,res,next){
    var email = req.body.email;
    var id = req.body.id;
    if(email && id)
    {
        var user = {id:id,email:email};
        req.session.user = user;
        res.cookie('user',{id:id,email:email},{expires: new Date(Date.now() + 86400 * 1000), httpOnly: true});
        res.json({ok:true});
    }
});


module.exports = router;